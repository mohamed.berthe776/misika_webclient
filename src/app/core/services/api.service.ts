import { Injectable } from '@angular/core';
import { appApiUrl } from '../services/apiUrlService';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public static userLogin: string = appApiUrl + '/api/Client/Connexion/';

}
