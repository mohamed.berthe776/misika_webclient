import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {


  loading!: boolean;
  loginForm!: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: LoginService
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });

  }


  get f() { return this.loginForm.value; }

  onSubmit() {
    let Request = this.f;
    this.loading = true;
    // this.service.Login(Request).subscribe(result => {
    //   this.loading = false;
    // });
    this.router.navigate(['Dashboard']);
  }
}
