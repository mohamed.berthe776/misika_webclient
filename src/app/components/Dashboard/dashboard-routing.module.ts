import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from '../layout/header/header.component';
import { LayoutComponent } from '../layout/layout.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  { path: 'Dashboard', component: LayoutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

//how do i redirect root to login in angular routing module ?
